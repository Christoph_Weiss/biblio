/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio;

/**
 *
 * @author Christoph Weiss
 */
public class Buch {
    private String buchnummer;
    private String sachgebiet;
    private String buchtitel;
    private String ort;
    private String erscheinungsjahr;
    private int verlag_id;
    
    public Buch(String buchnummer,String sachgebiet,String buchtitel,String ort,String erscheinungsjahr, int verlag_id){
        this.buchnummer = buchnummer;
        this.sachgebiet = sachgebiet;
        this.ort = ort;
        this.erscheinungsjahr = erscheinungsjahr;
        this.verlag_id = verlag_id;
    }
    

    public String getBuchnummer() {
        return buchnummer;
    }

    public void setBuchnummer(String buchnummer) {
        this.buchnummer = buchnummer;
    }

    public String getSachgebiet() {
        return sachgebiet;
    }

    public void setSachgebiet(String sachgebiet) {
        this.sachgebiet = sachgebiet;
    }

    public String getBuchtitel() {
        return buchtitel;
    }

    public void setBuchtitel(String buchtitel) {
        this.buchtitel = buchtitel;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getErscheinungsjahr() {
        return erscheinungsjahr;
    }

    public void setErscheinungsjahr(String erscheinungsjahr) {
        this.erscheinungsjahr = erscheinungsjahr;
    }

    public int getVerlag_id() {
        return verlag_id;
    }

    public void setVerlag_id(int verlag_id) {
        this.verlag_id = verlag_id;
    }
    
}
