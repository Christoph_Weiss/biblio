/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio;

/**
 *
 * @author Christoph Weiss
 */
public class Verlag {
    private int id;
   private String name;
   private String ort;

    public Verlag(int id, String name, String ort) {
        this.id = id;
        this.name = name;
        this.ort = ort;
        
    }
   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }
  
}
