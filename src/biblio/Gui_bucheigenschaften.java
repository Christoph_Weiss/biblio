/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 *
 * @author Christoph Weiss
 */
public class Gui_bucheigenschaften extends JFrame implements ActionListener{
    private JPanel mainPanel;
    private JLabel lb_title;
    private JLabel lb_buchnummer;
    private JLabel lb_sachgebiet;
    private JLabel lb_buchtitel;
    private JLabel lb_autor_nachname;
    private JLabel lb_autor_vorname;
    private JLabel lb_verlag;
    private JLabel lb_ort;
    private JLabel lb_erscheinungsJahr;
    
    private JTextField tf_buchnummer;
    private JTextField tf_sachgebiet;
    private JTextField tf_buchtitel;
    private JTextField tf_autor_nachname;
    private JTextField tf_autor_vorname;
    private JTextField tf_verlag;
    private JTextField tf_ort;
    private JTextField tf_erscheinungsJahr;
    
    private JButton btn_newData;
    private JButton btn_search;
    private JButton btn_edit;
    private JButton btn_delete;
    private JButton btn_back;
    
    public Gui_bucheigenschaften() {
        initalize();
    }

    private void initalize() {
        this.setSize(new Dimension(700, 700));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        mainPanel = new JPanel();
                
        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);
        
        lb_title = new JLabel();
        lb_title.setText("Buch-Eigenschaften");
        lb_title.setFont(new Font("Serif",Font.PLAIN,50));
        lb_title.setBounds(100,5,500,50);
        mainPanel.add(lb_title);
        
        lb_buchnummer = new JLabel();
        lb_buchnummer.setText("Buchnummer");
        lb_buchnummer.setFont(new Font("Serif",Font.PLAIN,15));
        lb_buchnummer.setBounds(50, 80, 150, 25);
        mainPanel.add(lb_buchnummer);
        
        tf_buchnummer = new JTextField();
        tf_buchnummer.setBounds(150, 80, 500, 25);
        mainPanel.add(tf_buchnummer);
        
        lb_sachgebiet = new JLabel();
        lb_sachgebiet.setText("Sachgebiet");
        lb_sachgebiet.setFont(new Font("Serif",Font.PLAIN,15));
        lb_sachgebiet.setBounds(50, 140, 200, 25);
        mainPanel.add(lb_sachgebiet);
        
        tf_sachgebiet = new JTextField();
        tf_sachgebiet.setBounds(150, 140, 500, 25);
        mainPanel.add(tf_sachgebiet);
        
        lb_buchtitel = new JLabel();
        lb_buchtitel.setText("Buchitel");
        lb_buchtitel.setFont(new Font("Serif",Font.PLAIN,15));
        lb_buchtitel.setBounds(50, 200, 200, 25);
        mainPanel.add(lb_buchtitel);
        
        tf_buchtitel = new JTextField();
        tf_buchtitel.setBounds(150, 200, 500, 25);
        mainPanel.add(tf_buchtitel);
        
        lb_autor_nachname = new JLabel();
        lb_autor_nachname.setText("Autor_Nachname");
        lb_autor_nachname.setFont(new Font("Serif",Font.PLAIN,15));
        lb_autor_nachname.setBounds(50, 260, 150, 25);
        mainPanel.add(lb_autor_nachname);
        
        tf_autor_nachname = new JTextField();
        tf_autor_nachname.setBounds(170, 260, 170, 25);
        mainPanel.add(tf_autor_nachname);
        
        lb_autor_vorname = new JLabel();
        lb_autor_vorname.setText("Vorname");
        lb_autor_vorname.setFont(new Font("Serif",Font.PLAIN,15));
        lb_autor_vorname.setBounds(400, 260, 150, 25);
        mainPanel.add(lb_autor_vorname);
        
        tf_autor_vorname = new JTextField();
        tf_autor_vorname.setBounds(500, 260, 150, 25);
        mainPanel.add(tf_autor_vorname);
        
        lb_verlag = new JLabel();
        lb_verlag.setText("Verlag");
        lb_verlag.setFont(new Font("Serif",Font.PLAIN,15));
        lb_verlag.setBounds(50, 320, 200, 25);
        mainPanel.add(lb_verlag);
        
        tf_verlag = new JTextField();
        tf_verlag.setFont(new Font("Serif",Font.PLAIN,15));
        tf_verlag.setBounds(100, 320, 200, 25);
        mainPanel.add(tf_verlag);
        
        lb_ort = new JLabel();
        lb_ort.setText("Ort");
        lb_ort.setFont(new Font("Serif",Font.PLAIN,15));
        lb_ort.setBounds(350, 320, 200, 25);
        mainPanel.add(lb_ort);
        
        tf_ort = new JTextField();
        tf_ort.setBounds(400, 320, 200, 25);
        mainPanel.add(tf_ort);
        
        lb_erscheinungsJahr = new JLabel();
        lb_erscheinungsJahr.setText("Erscheinungsjahr");
        lb_erscheinungsJahr.setFont(new Font("Serif",Font.PLAIN,15));
        lb_erscheinungsJahr.setBounds(50, 380, 200, 25);
        mainPanel.add(lb_erscheinungsJahr);
        
        tf_erscheinungsJahr = new JTextField();
        tf_erscheinungsJahr.setBounds(180, 380, 100, 25);
        mainPanel.add(tf_erscheinungsJahr);
        
        btn_newData = new JButton();
        try {
        Image img = ImageIO.read(getClass().getResource("../pic/newData.png"));
        btn_newData.setIcon(new ImageIcon(img));
      } catch (Exception ex) {
        System.out.println(ex);
      }
        btn_newData.setOpaque(false);
        btn_newData.setContentAreaFilled(false);
        btn_newData.setBorderPainted(false);
        btn_newData.setBorder(BorderFactory.createEmptyBorder());
        btn_newData.setBounds(150, 450, 50, 50);
        btn_newData.addActionListener(this);
        mainPanel.add(btn_newData);
        
        
        btn_search = new JButton();
        try {
        Image img = ImageIO.read(getClass().getResource("../pic/search.png"));
        btn_search.setIcon(new ImageIcon(img));
      } catch (Exception ex) {
        System.out.println(ex);
      }
        btn_search.setOpaque(false);
        btn_search.setContentAreaFilled(false);
        btn_search.setBorderPainted(false);
        btn_search.setBorder(BorderFactory.createEmptyBorder());
        btn_search.setBounds(250, 450, 50, 50);
        btn_search.addActionListener(this);
        mainPanel.add(btn_search);
        
        btn_edit = new JButton();
        try {
        Image img = ImageIO.read(getClass().getResource("../pic/edit.png"));
        btn_edit.setIcon(new ImageIcon(img));
      } catch (Exception ex) {
        System.out.println(ex);
      }
        btn_edit.setBorder(BorderFactory.createEmptyBorder());
        btn_edit.setOpaque(false);
        btn_edit.setContentAreaFilled(false);
        btn_edit.setBorderPainted(false);
        btn_edit.addActionListener(this);
        btn_edit.setBounds(350, 450, 50, 50);
        mainPanel.add(btn_edit);
        
        btn_delete = new JButton();
        try {
        Image img = ImageIO.read(getClass().getResource("../pic/edit.png"));
        btn_delete.setIcon(new ImageIcon(img));
      } catch (Exception ex) {
        System.out.println(ex);
      }
        btn_delete.setBorder(BorderFactory.createEmptyBorder());
        btn_delete.setOpaque(false);
        btn_delete.setContentAreaFilled(false);
        btn_delete.setBorderPainted(false);
        btn_delete.setBounds(450, 450, 50, 50);
        btn_delete.addActionListener(this);
        mainPanel.add(btn_delete);
        
        btn_back = new JButton();
        try {
        Image img = ImageIO.read(getClass().getResource("../pic/back.png"));
        btn_back.setIcon(new ImageIcon(img));
      } catch (Exception ex) {
        System.out.println(ex);
      }
        btn_back.setBorder(BorderFactory.createEmptyBorder());
        btn_back.setOpaque(false);
        btn_back.setContentAreaFilled(false);
        btn_back.setBorderPainted(false);
        btn_back.addActionListener(this);
        btn_back.setBounds(550, 450, 50, 50);
        mainPanel.add(btn_back);
        
        
        
    }
    public void actionPerformed(ActionEvent e) {
//      if(e.getSource() == btn_Autorsuche){
//          
//      } 
    }
    
}
