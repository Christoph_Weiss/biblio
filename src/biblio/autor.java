/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio;

/**
 *
 * @author Christoph Weiss
 */
public class autor {
    private int id;
    private String vorname;
    private String nachname;
    private String geburtsort;
    private String geburtsdatum;
    
    public autor(int id,String vorname,String nachname, String geburtsort, String geburtsdatum){
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.geburtsort = geburtsort;
        this.geburtsdatum = geburtsdatum;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getGeburtsort() {
        return geburtsort;
    }

    public void setGeburtsort(String geburtsort) {
        this.geburtsort = geburtsort;
    }

    public String getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(String geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }
    
    
}
